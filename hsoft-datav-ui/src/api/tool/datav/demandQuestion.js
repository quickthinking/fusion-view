import request from '@/utils/request'

// 查询需求问卷列表
export function listQuestion(query) {
  return request({
    url: '/datav/demand/list',
    method: 'get',
    params: query
  })
}

// 查询需求问卷详细
export function getQuestion(id) {
  return request({
    url: '/datav/demand/' + id,
    method: 'get'
  })
}

// 新增需求问卷
export function addQuestion(data) {
  return request({
    url: '/datav/demand',
    method: 'post',
    data: data
  })
}

// 修改需求问卷
export function updateQuestion(data) {
  return request({
    url: '/datav/demand',
    method: 'put',
    data: data
  })
}

// 删除需求问卷
export function delQuestion(id) {
  return request({
    url: '/datav/demand/' + id,
    method: 'delete'
  })
}

// 导出需求问卷
export function exportQuestion(query) {
  return request({
    url: '/datav/demand/export',
    method: 'get',
    params: query
  })
}

// 模板类型统计数据
export function templateTypeAnalysis() {
  return request({
    url: '/datav/demand/templateType/count',
    method: 'get',
  })
}

// 模板需求统计数据
export function templateDemandAnalysis() {
  return request({
    url: '/datav/demand/templateDemand/count',
    method: 'get',
  })
}

// 岗位类型统计数据
export function postTypeAnalysis() {
  return request({
    url: '/datav/demand/postType/count',
    method: 'get',
  })
}

// 岗位类型-模板需求统计数据
export function postAndDemandAnalysis() {
  return request({
    url: '/datav/demand/postAndDemand/count',
    method: 'get',
  })
}